#### 介绍

WinBoard（GNU 著名国际象棋图形界面软件）＋Stockfish（国际主流人工智能引擎）中文汉化及改进项目，本软件包含国际象棋的对弈、训练及分析功能：

 ![国际象棋](.gitee/chess.png)

#### 使用说明

**声明** ：本程序和代码的发行，遵循原程序所遵循的GNU GPL v3发行及使用协议。

本软件包中包含 GNU Winboard 4.91 + Stockfish 16 开源软件程序包，在原版代码基础之上增加以下功能改进：

 - 为 Stockfish 增加悔棋指令支持；
 - 删除 Winboard 界面对齐及最前端显示；
 - 修改了 Winboard 各种不合理的默认设置；
 - 分析模式下暂停继续键可以控制引擎的运行；
 - 重新翻译了阅读起来更友好的中文界面翻译；

以及其他大量改进

本改进项目对用户使用本软件所产生的任何问题及损失，不承担任何形式的责任与义务，也不负责问题解答和使用服务，项目代码以开源形式发布在：

 - https://www.gitee.com/neuks/winboard

我们欢迎各位国际象棋爱好者下载、编译、研究、提交改进。

#### 如何编译

本项目使用 Windows 上的 GCC 进行编译，可以使用 MinGW/MinGW64 或者 MSYS2、Cygwin 编译器编译本项目。

1. Clone 本仓库
2. cd src
3. make
4. cd ..\polyglot
5. make
6. cd ..\stockfish
7. make -j ARCH=x86-64-modern
8. cd ..\network
9. make
10. cd ..\winboard
11. make
12. 清理临时文件：make clean

#### 如何参与

1.  Fork 本仓库
2.  新建分支
3.  提交代码
4.  新建 Pull Request

